clc
clear

%% 5.1
%% y = n0/(1+n0*alpha*t) ==> 1/y = (1+n0*alpha*t)/n0 = 1/n0 + alpha*t

t = (0:10)';
n = [5.03; 4.71; 4.40; 3.97; 3.88; 3.62; 3.30; 3.15;
    3.08; 2.92; 2.70]*1e-4;

%% 5.2

y = 1./n;
A = [ones(size(t)), t];
c = A\y;

n0 = 1/c(1);
alfa = c(2);

fprintf('  n0 = %3.4f \n',n0)
fprintf('alfa = %3.4f \n',alfa)

%% 5.3

N = @(t) n0./(1 + n0*alfa*t);

plot(t,n,'x')
hold on
tt = linspace(0,10,110);
plot(tt,N(tt))
hold off

xlabel('Tiempo [s]','FontSize',14)
ylabel('Concentracin de iones','FontSize',14)
legend('Datos','Aprox. por min. cuad.')
title('Ejercicio 5','FontSize',20)