clc
clear

a = (0:8)';
ts = [12; 10.5; 10.0; 8.0; 7.0; 8.0; 7.5; 8.5; 9.0];

%% 1.2

% Construya la matriz A y la parte derecha y del sistema
A = [a.^2 a ones(size(a))];
y = ts;

% Resuelva el sistema Ac = y en el sentido de los m�nimos cuadrados
c = A\y;

% Grafique en un mismo gr�fico los pares en la Tabla y el polinomio
% obtenido evaluado en 100 puntos entre 0 y 8

plot(a,ts,'o')
hold on
aa = linspace(0,8,100);
plot(aa,polyval(c,aa))
hold off

xlabel('Aditivo [g]','FontSize',14)
ylabel('Tiempo de secado [h]','FontSize',14)
legend('Datos','Ajuste polinomial')
title('Ejercicio 1','FontSize',20)

% Basados en el polinomio resultante, �qu� cantidad de aditivo resulta en
% tiempo m�nimo de secado? �Cu�l es el tiempo m�nimo de secado?

amin = -c(2)/(2*c(1));          %amin = -b/2a
tmin = polyval(c,amin);         %tmin = p(-b/2a)