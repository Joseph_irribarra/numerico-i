clear all ; close all; clc

x = 0:8 ; 
y = [12,10.5,10,8,7,8,7.5,8.5,9]' ; 
#{
ajustamos los puntos anteriores mediante un polinomio de grado 2
de la forma p(x) = ax^2 +bx +c usando minimos cuadrados
debemos minimizar el error en el sitema de ecuaciones Az = y
donde A es de la forma:
A = [0 0 1
    1 1 1
    4 2 1
    9 3 1
    16 4 1
    . . .
    . . .
    . . .
    64 8 1]
z = [a
     b
     c]
#}

# creamos la matriz A y el vector z:
m = length(x) ;
n = 3 ;

A = ones(m, n) ;
A(:,1) = x'.**2;
A(:,2) = x' ;

# para encontrar el vector z debemos resolver el sistema 
# A^tAz = A^ty
z = (A'*A)\(A'*y)

# graficamos
xx = linspace(0,8) ;
hold on; 
plot(xx,polyval(z,xx))
plot(x,y,'*r')
legend('polinomio','puntos')

t_min = min( polyval(z,xx) ) ;
disp('el tiempo min de secado es:')
disp(t_min)

pos = find(polyval(z,xx) == t_min ) ;
x_min = xx(pos) ;
disp('el aditivo en el tiempo min es:')
disp(x_min)




