clc
clear

t = [.5; 1; 2; 3; 4];
pt = [7; 5.2; 3.8; 3.2; 2.5];

%% 2.2

% Construya la matriz A y parte derecha y del sistema
A = [exp(-1.5*t), exp(-0.3*t), exp(-0.05*t)];
y = pt;

% Resuelva el sistema Ac = y en el sentido de los m�nimos cuadrados
c = A\y;

% Grafique en un mismo gr�fico los pares en la Tabla y la funci�n p(t)
% obtenida

p = @(t) c(1)*exp(-1.5*t) + c(2)*exp(-0.3*t) + c(3)*exp(-0.05*t);

plot(t,pt,'o')
hold on
tt = linspace(0,6);
plot(tt,p(tt))
hold off

xlabel('Tiempo [h]','FontSize',14)
ylabel('Miles de microorganismos [-]','FontSize',14)
legend('Datos','Ajuste funcional')
title('Ejercicio 2','FontSize',20)

% En base a la funci�n obtenida, �cu�l es el n�mero de microorganismos que
% hab�a en la muestra inicialmente? �y despu�s de una hora y media? �y
% despu�s de 5 horas y media?

mo_inicial = p(0)
mo_hraymed = p(1.5)
mo_5hrymed = p(5.5)

