clc
clear

x = [1; 2; 5; 15; 25; 30; 35; 40];
y = [99; 95; 85; 55; 30; 24; 20; 15];

%% 3.1, 3.2

%%% Modelo 1
% log(y) = log(alfa)*1 + log(beta)*x

b = log(y);
A = [ones(size(x)), x];
c = A\b;
alfa1 = exp(c(1));
beta1 = exp(c(2));
err1 = norm(b - A*c,2);

fprintf('Modelo 1: y = a*b^x \n');
fprintf('a = %6.4f\nb = %6.4f \n',alfa1,beta1);
fprintf('||b - Ax||_2 = %6.4e \n\n',err1);

%%% Modelo 2
% log10(y) - log10(100 - x) = log10(a�fa) + beta*x

b = log10(y) - log10(100 - x);
A = [ones(size(x)), x];
c = A\b;
alfa2 = 10^c(1);
beta2 = c(2);
err2 = norm(b - A*c,2);

fprintf('Modelo 2: y = a*(100-x)*10^(bx) \n');
fprintf('a = %6.4f\nb = %6.4f \n',alfa2,beta2);
fprintf('||b - Ax||_2 = %6.4e \n\n',err2);

%% 3.3

fm1 = @(x) alfa1.*beta1.^x;
fm2 = @(x) alfa2.*(100 - x).*10.^(beta2*x);

plot(x,y,'x')
hold on
xx = linspace(0,50);
plot(xx,fm1(xx),'k-');
plot(xx,fm2(xx),'r-');
hold off

xlabel('Miles de millas recorridas [-]','FontSize',14)
ylabel('Porcentaje �til [%]','FontSize',14)
legend('Datos','Modelo 1', 'Modelo 2')
title('Ejercicio 3','FontSize',20)

%% 3.4

% Con el mejor modelo, estime qu� porcentaje de las llantas radiales del
% fabricante durar�n 45 (mil) millas y 50 (mil) millas.

x45 = fm2(45);
x50 = fm2(50);

fprintf('  Estimaciones\n');
fprintf(' ----------------- \n');
fprintf('| Millas | %% util |\n');
fprintf(' ----------------- \n');
fprintf('|   45   |%8.2f|\n',x45);
fprintf('|   50   |%8.2f|\n',x50);
fprintf(' ----------------- \n');
 