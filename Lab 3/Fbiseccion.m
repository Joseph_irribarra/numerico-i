function xr = Fbiseccion(f,xa,xb)
  xk =[];                              %  inicializamos xk
  epsilon =  10^(-8);   
  step = 0              % tolerancia                                   
  while abs(xa-xb)>=epsilon  && step <= 349 % se ejecuta met biseccion
      step += 1
      xr=(xa+xb)/2;                   % encontramos el punto medio                    % nos detenemos 1 seg en la iteracion
      if f(xa)*f(xr)<0                % vemos los casos posibles a tener para un nuevo intervalo
          xb=xr;                      %
      else
          xa=xr;                      %
      endif
  endwhile