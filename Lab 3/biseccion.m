clear all; close all; clc;
%% Implementación particular del método de la bisección
f=inline('cos(x.^2+1)./(x+1)','x'); % Resuma lo que se hacen en estas líneas
ezplot(f); grid on; hold all;       % grafica mi funcion inline
xa=0; xb=1.5;                         %  fijamos un intervalo donde halla un cambio de signo en la funcion      
xk =[];                              %  inicializamos xk
epsilon =  10^(-8);               % tolerancia                                   
while abs(xa-xb)>=epsilon % se ejecuta met biseccion
    plot([xa,xb],f([xa,xb]),'*k');  % ploteamos nuestro nuevi intervalo
    xr=(xa+xb)/2;                   % encontramos el punto medio
    plot(xr,f(xr),'*r'); 
    pause(1)    % ploteamos f(xr)                       % nos detenemos 1 seg en la iteracion
    if f(xa)*f(xr)<0                % vemos los casos posibles a tener para un nuevo intervalo
        xb=xr;                      %
    else
        xa=xr;                      %
    end
    xk = [xk;xr] ;                   % guarda los puntos medios de todos los pasos en un vector
end
error=abs(f(xr)-0)                  % 
