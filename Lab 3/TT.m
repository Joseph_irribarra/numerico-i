function x = TT(kappa, tt)
  x = zeros(length(tt),1);
  T = inline( '(10.2 + 1/(2*k))*exp(-k*(t-20)) + (t+24)/2 - 1/(2*k)','k','t' );
  
  for i = 1:length(tt)
    x(i) = T(kappa, tt(i)) - 36.5 ;
  endfor
  
  