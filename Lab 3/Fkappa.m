function fk = Fkappa(kappa)
  fk = zeros(length(kappa),1) ;
  T = inline( '(10.2 + 1/(2*k))*exp(-k*(t-20)) + (t+24)/2 - 1/(2*k)','k','t' );
  for i = 1:length(kappa)
    fk(i) = T(kappa(i),21) - 29.4;
  endfor
  
    