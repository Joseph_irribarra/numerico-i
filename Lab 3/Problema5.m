# 1)
# la funcion creada se llama Fkappa

# 2)
# creamos el vector de 100 elementos entre 0.1 y 0.5
xx = linspace(0.1, 0.5) ;
# graficamos la funcion de  1)
plot(xx,Fkappa(xx))
grid on ; hold on ;

# del grafico notamos que existe una raiz de la funcion
# con un valor cercano a 0.35

kappa = fzero(@Fkappa ,0.35) ;
plot(kappa, Fkappa(kappa),'*r')
disp('el valor de kappa es:')
disp(kappa)

# 3)
# la funcion se llama TT

# 4) determinamos la hora de muerte.
close all; hold on; grid on;
tt = linspace(16,20) ;
plot(tt,TT(kappa,tt))

t0 = fzero(@(tt)TT(kappa,tt), 19)

# notemos que para la funcion T definida en el .pdf
T = inline( '(10.2 + 1/(2*k))*exp(-k*(t-20)) + (t+24)/2 - 1/(2*k)','k','t' );
# bajo los parametros encontrados :
# kappa =  0.33100
# t0 =  18.958 : tiempo de muerte de la persona
# cumple que T(21) = 29.4
# T(hora de muerte = t0) = 36.5
T(kappa,21)
T(kappa, t0)


