clear all; close all; clc ;
x = [0
0.5
1
1.5
2
2.5
3
3.5
4
4.5
5
5.5
6
6.5
7
7.5
8
8.5
9
9.5
10]' ;
y = [2.5
2.8
3
3.3
3.8
4.8
4.8
5
4.8
4.7
4.7
4.5
4.5
4.4
4.3
4.4
4.2
4
4.1
4.3
3.5]' ;

# encontramos el polinomio que interpole nuestros datos
coef = polyfit(x,y, 21) ; 
# graficamos
xx = linspace(0,10,200) ;
grid on; hold on;
plot(x,y,'*r')
plot(xx, polyval(coef,xx), '-k')
legend('puntos','aprox polinomial')
title('aprox polinomial')
pause(5)
close all
# es claro que no es una buena aproximacion.

# b) la funcion se llama "s"
sv = s(xx) ; 
# c)
figure
grid on; hold on;
plot(x,y,'*k')
plot(xx, sv, '-r')
legend('puntos','spline')
title('spline')
