clear all; clc ;
#################### a) ######################
# ln(x) = log(x) en octave
puntos = 1:3 ;
f = log(puntos) ;
# como tenemos 3=n+1 puntos, elegimos un pol de grado n=2
# para la interpolacion.
coef = polyfit(puntos, f, 2) ; 

#################### b) ######################
x = linspace(1,3,200) ; 
p = polyval(coef, x) ;
grid on; hold on;
plot(puntos, f, '*r')
plot(x, p, '-k')
plot(x, log(x), '-b')
legend('Puntos','Polinomio','ln')
pause(2)
close all

#################### c) ######################
# como f(x) = ln(x) => f'''(x) = 2/x³
# sea dif = |f(xi) - p(xi)|

h = 1/50;
xi = zeros(1,101) ; 
fmax = xi ;
f3 = inline("2./x.**3","x") ; # f3 = f'''

for i = 0:100
  xi(i+1) = 1+i*h ;
  fmax(i+1) = abs( (xi(i+1)-1)*(xi(i+1)-2)*(xi(i+1)-3) )/6 ;
endfor

fmax = max( f3(xi) )*fmax ;

f_i = log(xi) ;
p_i = polyval(coef, xi) ;
dif = abs(f_i-p_i) ;
figure
hold on;
plot(xi,dif, '-r')
plot(xi,fmax, 'b')
legend('diferencia','maximo')










