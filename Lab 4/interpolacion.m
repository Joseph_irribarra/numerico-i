clear all; clc ;
# cargamos los datos
data = load("data.mat") ; 
%{
este paso es opcional, solo guardamos el 
contenido de la estructura en dos variables.
%}
longitud = data.longitud ;
tiempo = data.tiempo ;
%{
a)
encontramos los coef de nuestro polinomio
que se ajuste a nuestros datos.
%}
coef = polyfit( tiempo, longitud, length(tiempo)-1) ;
%{
b)
Graficamos el polinomio encontrado y los datos
crearemos una variable x con los puntos a evaluar
%}
x = linspace(min(tiempo), max(tiempo)) ;
hold on ; grid on ;
plot(x, polyval(coef, x)) ; 
plot(tiempo, longitud, '*r') ;