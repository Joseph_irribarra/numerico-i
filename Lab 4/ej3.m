clear all; clc ;
x = [0.9 1.3 1.9 2.1 2.6 3 3.9 4.4 4.7 5 6 7 8 9.2 10.5 11.3 11.6 12 12.6 13 13.3] ;
y = [1.3 1.5 1.85 2.1 2.6 2.7 2.4 2.15 2.05 2.1 2.25 2.3 2.25 1.95 1.4 0.9 0.7 0.6 0.5 0.4 0.25] ;
# a) encontramos el polinomio que interpola los puntos
coef = polyfit(x,y, length(x) - 1) ; 
# b) graficamos el polinomio
xx = linspace(0.9, 13.3) ;
hold on;
plot(xx, polyval(coef,xx), '-k') 
plot(x,y,'*r')
pause(4)
close all

# c) graficamos el spline
sp = spline(x,y) ; 
hold on;
plot(xx, ppval(sp,xx), '-k')
plot(x,y,'*r')






